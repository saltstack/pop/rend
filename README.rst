==========
!ARCHIVED!
==========

This project has been archived, along with all other POP and Idem-based projects.

* For more details: `Salt Project Blog - POP and Idem Projects Will Soon be Archived <https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/>`__

====
rend
====

.. image:: https://img.shields.io/badge/made%20with-pop-teal
   :alt: Made with pop, a Python implementation of Plugin Oriented Programming
   :target: https://pop.readthedocs.io/

.. image:: https://img.shields.io/badge/docs%20on-vmware.gitlab.io-blue
   :alt: Documentation is published with Sphinx on GitLab Pages via vmware.gitlab.io
   :target: https://vmware.gitlab.io/pop/rend/en/latest/index.html

.. image:: https://img.shields.io/badge/made%20with-python-yellow
   :alt: Made with Python
   :target: https://www.python.org/

A collection of tools to render text files into data structures.

About
=====

Rend is the renderer collection used by ``pop`` and ``pop`` derived projects.
It allows for text files of multiple types to be read and rendered into
consistent data structures. These files also get the benefit of having
access to specific components of the hub.

* `rend source code <https://gitlab.com/vmware/pop/rend>`__
* `rend documentation <https://vmware.gitlab.io/pop/rend/en/latest/index.html>`__

What is POP?
------------

This project is built with `pop <https://pop.readthedocs.io/>`__, a Python-based
implementation of *Plugin Oriented Programming (POP)*. POP seeks to bring
together concepts and wisdom from the history of computing in new ways to solve
modern computing problems.

For more information:

* `Intro to Plugin Oriented Programming (POP) <https://pop-book.readthedocs.io/en/latest/>`__
* `pop-awesome <https://gitlab.com/vmware/pop/pop-awesome>`__
* `pop-create <https://gitlab.com/vmware/pop/pop-create/>`__

Usage
=====

View the `rend documentation <https://vmware.gitlab.io/pop/rend/en/latest/index.html>`__
for more information.

Acknowledgements
================

* `Img Shields <https://shields.io>`__ for making repository badges easy.
