"""
    tests.unit.rend.test_json
    ~~~~~~~~~~~~~~

    Unit tests for the json renderer
"""
import pytest

import rend.exc
import rend.rend.json_file


def test_json_string(mock_hub, hub):
    """
    test rend.json.render renders correctly when passed a string
    """
    data = '{"test1": "one"}'
    mock_hub.rend.json.render = hub.rend.json.render
    ret = mock_hub.rend.json.render(data)
    assert ret == {"test1": "one"}


@pytest.mark.asyncio
async def test_json_bytes(mock_hub, hub):
    """
    test rend.json.render renders correctly with bytes data
    """
    mock_hub.rend.json.render = hub.rend.json.render
    ret = mock_hub.rend.json.render(b'{"test1": "one"}')
    assert ret == {"test1": "one"}


@pytest.mark.asyncio
async def test_json_error(mock_hub, hub):
    """
    test rend.json.render when there is an error
    """
    mock_hub.rend.json.render = hub.rend.json.render
    with pytest.raises(rend.exc.RenderException) as exc:
        mock_hub.rend.json.render('{"test1"}}')
    assert (
        exc.value.args[0]
        == "Json render error: Expecting ':' delimiter on line: 1 column: 9"
    )
